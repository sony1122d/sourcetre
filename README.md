<!DOCTYPE html>
<html>
<head>
	<title>Dashboard sign in</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="login.css">
</head>
<body>
<div class="container">
	<div class="jumbotron">
		<h4>Welcome</h4>
		<h3>QUICKTICKETS DASHBOARD </h3>
	<FORM>
		<div class="inner-addon left-addon">
			<br>User name </br>	
<SPAN><i class="glyphicon glyphicon-user"></i></SPAN>
		<input type="text" name="User name">
	</div>
		
		<br>Password<br>
		<input type="Password" name="Password">
		<br>

		<button type="button">Sign in</button></br>
		<br>
		<input type="checkbox" name="checkbox"> Remember me
	</br>
	</FORM>
		
	</div>
	
</div>



<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>